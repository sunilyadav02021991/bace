import { Component, ViewChild, Renderer2 } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
import { NgAlertService } from 'webdunia-toast';
import { DialogOverviewExampleDialogComponent } from './dialog-overview-example-dialog/dialog-overview-example-dialog.component';

import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';


export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        height: '100px',
        opacity: 1,
        backgroundColor: 'gray'
      })),
      state('closed', style({
        height: '50px',
        opacity: 0.5,
        backgroundColor: 'green'
      })),
      transition('open => closed', [
        animate('1s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ])
    ])
  ]
})

export class AppComponent {
  title = 'animations';
  isOpen = true;

  centered = false;
  disabled = false;
  unbounded = false;

  radius: number;
  color: string;
  animal: string;
  myName: string;

  // charts related

  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A' },
  ];
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions: (ChartOptions) = {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'pie';
  // line, bar, pie, Doughnut Chart, Radar Chart,
  // Polar Area Chart, Bubble Chart and Scatter Chart ||https://www.npmjs.com/package/ng2-charts
  public lineChartPlugins = [];

  menuList : any =  [
    {
      "menuId": 1,
      "name": "Dashboard",
      "menuUrl": "/dashboard",
      "authorities": [
        {
          "aaId": 125,
          "name": "CG Battery Livestatus List",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/goods/livestatus/analoginput/battery/status",
          "authorityName": "CG_BATTERY_LIVESTATUS_LIST"
        },
        {
          "aaId": 123,
          "name": "CG Door Livestatus List",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/goods/livestatus/digitalswitch/door/status",
          "authorityName": "CG_DOOR_LIVESTATUS_LIST"
        },
        {
          "aaId": 126,
          "name": "CG Power Livestatus List",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/goods/livestatus/asset/powerstatus",
          "authorityName": "CG_POWER_LIVESTATUS_LIST"
        },
        {
          "aaId": 124,
          "name": "CG Temperature Livestatus List",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/goods/livestatus/analoginput/temperature/status",
          "authorityName": "CG_TEMPERATURE_LIVESTATUS_LIST"
        },
        {
          "aaId": 133,
          "name": "DG live status list",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/goods/livestatus/asset/dgstatus",
          "authorityName": "DG_LIVE_STATUS_LIST"
        },
        {
          "aaId": 97,
          "name": "Get Current Route History",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/reports/routes/{id}",
          "authorityName": "GET_CURRENT_ROUTE_HISTORY"
        },
        {
          "aaId": 95,
          "name": "Get Device Alerts",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/devices/alerts",
          "authorityName": "GET_DEVICE_ALERTS"
        },
        {
          "aaId": 96,
          "name": "Get Device Alerts Duration",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/devices/AlertsDuration",
          "authorityName": "GET_DEVICE_ALERTS_DURATION"
        },
        {
          "aaId": 94,
          "name": "Get Device CurrentStatus",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/devices/currentstatus",
          "authorityName": "GET_DEVICE_CURRENTSTATUS"
        },
        {
          "aaId": 98,
          "name": "Get Geofence By Provision Id",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/geofences/{id}",
          "authorityName": "GET_GEOFENCE_BY_PROVISION_ID"
        },
        {
          "aaId": 135,
          "name": "Get tracking interval",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/devices/gettrackinginterval",
          "authorityName": "GET_TRACKING_INTERVAL"
        },
        {
          "aaId": 12,
          "name": "Get User Identity",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/users/identity",
          "authorityName": "READ_USER_IDENTITY"
        },
        {
          "aaId": 136,
          "name": "Update tracking interval",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/devices/updatetrackinginterval",
          "authorityName": "UPDATE_TRACKING_INTERVAL"
        },
        {
          "aaId": 73,
          "name": "Widget-Alerts-Status",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/aggregation/alerts",
          "authorityName": "READ_ALERTS_STATUS"
        },
        {
          "aaId": 121,
          "name": "Widget-CG Battery Status Aggregation",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/goods/aggregation/analoginput/battery/status",
          "authorityName": "CG_BATTERY_STATUS_AGGREGATION"
        },
        {
          "aaId": 119,
          "name": "Widget-CG Door Status Aggregation",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/goods/aggregation/digitalswitch/door/status",
          "authorityName": "CG_DOOR_STATUS_AGGREGATION"
        },
        {
          "aaId": 122,
          "name": "Widget-CG Power Status Aggregation",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/goods/aggregation/asset/powerstatus",
          "authorityName": "CG_POWER_STATUS_AGGREGATION"
        },
        {
          "aaId": 120,
          "name": "Widget-CG Temperature Status Aggregation",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/goods/aggregation/analoginput/temperature/status",
          "authorityName": "CG_TEMPERATURE_STATUS_AGGREGATION"
        },
        {
          "aaId": 72,
          "name": "Widget-Device-Status",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/aggregation/devicestatus",
          "authorityName": "READ_DEVICE_STATUS"
        },
        {
          "aaId": 132,
          "name": "Widget-DG status",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/goods/aggregation/asset/dgstatus",
          "authorityName": "DG_STATUS"
        },
        {
          "aaId": 4,
          "name": "Widget-Live Track",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/devices/users",
          "authorityName": "READ_LIVE_TRACK"
        },
        {
          "aaId": 5,
          "name": "Widget-Recent Added Users",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/users",
          "authorityName": "READ_RECENT_USER"
        },
        {
          "aaId": 6,
          "name": "Widget-Recent Alerts",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/users/{email}/devices/alerts",
          "authorityName": "READ_RECENT_ALERT"
        },
        {
          "aaId": 127,
          "name": "Widget-SLI Status Aggregation",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/aggregation/heavyvehicle/slidata",
          "authorityName": "SLI_STATUS_AGGREGATION"
        },
        {
          "aaId": 1,
          "name": "Widget-Vehicle Status",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/aggregation/vehiclestatus",
          "authorityName": "READ_VEHICLE_STATUS"
        },
        {
          "aaId": 2,
          "name": "Widget-User Aggregaion",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/aggregation/user",
          "authorityName": "READ_USER_AGGREGATION"
        },
        {
          "aaId": 3,
          "name": "Widget-Site Aggregation",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/aggregation/site",
          "authorityName": "READ_SITE_AGGREGATION"
        },
        {
          "aaId": 202,
          "name": "Get Device Alerts by ALC Code",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/ams/alerts/alc/{id}",
          "authorityName": "GET_DEVICE_ALERTS_BY_ALC_CODE"
        },
        {
          "aaId": 212,
          "name": "Get Geofence By Id",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/geofence/{id}",
          "authorityName": "GET_GEOFENCE_BY_ID"
        },
        {
          "aaId": 234,
          "name": "Get Route Details by Id",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/route/{alphanumspecial}",
          "authorityName": "GET_ROUTE_DETAILS_BY_ID"
        },
        {
          "aaId": 235,
          "name": "Get Cooler Live Status",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/livestatus/asset",
          "authorityName": "GET_COOLER_LIVE_STATUS"
        },
        {
          "aaId": 211,
          "name": "Search Vehicle",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/search/assets",
          "authorityName": "SEARCH_VEHICLE"
        },
        {
          "aaId": 319,
          "name": "Assets Model: Widget-Alert Aggregation",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/v1/ams/aggregation/alertCounts",
          "authorityName": "ALERT_AGGREGATION"
        },
        {
          "aaId": 318,
          "name": "Assets Model: Widget-Alert List",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/v1/ams/alerts",
          "authorityName": "ASSET_ALERT_LIST"
        },
        {
          "aaId": 314,
          "name": "Assets Model: Widget-Asset Aggregation",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/v1/aggregations/assetStatusCounts",
          "authorityName": "ASSET_AGGREGATION"
        },
        {
          "aaId": 313,
          "name": "Assets Model: Widget-Asset Live Status",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/v1/live/assetStatuses",
          "authorityName": "ASSET_LIVE_STATUS"
        },
        {
          "aaId": 316,
          "name": "Assets Model: Widget-Device Aggregation",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/v1/aggregation/deviceStatusCounts",
          "authorityName": "DEVICE_AGGREGATION"
        },
        {
          "aaId": 315,
          "name": "Assets Model: Widget-Device Live Status",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/v1/live/deviceStatuses",
          "authorityName": "DEVICE_LIVE_STATUS"
        },
        {
          "aaId": 317,
          "name": "Assets Model: Widget-SLI Aggregation",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/v1/aggregations/sliStatusCounts",
          "authorityName": "SLI_AGGREGATION"
        },
        {
          "aaId": 287,
          "name": "Assets Model: Get Geofence by Id",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/platform/geofences/{alphanumspecial}",
          "authorityName": "GET_GEOFENCE_BY_ID_PLATFORM"
        },
        {
          "aaId": 495,
          "name": "Get Geofence Live Statistics",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/v1/aggregation/geofencestatuscounts",
          "authorityName": "GET_GEOFENCE_LIVE_STATISTICS"
        },
        {
          "aaId": 504,
          "name": "Assets Model: Get Geofences",
          "menuId": 1,
          "parentAAId": 0,
          "actionUrl": "/platform/geofences",
          "authorityName": "GET_GEOFENCES_PLATFORM"
        }
      ]
    },
    {
      "menuId": 290,
      "name": "Fundamentals",
      "menuUrl": "/fundamentals",
      "authorities":[]
    },
    {
      "menuId": 49,
      "name": "Alert Subscribe",
      "menuUrl": "/ams/alerts/subscribe",
      "authorities": [
        {
          "aaId": 179,
          "name": "Assets by User Id",
          "menuId": 49,
          "parentAAId": 0,
          "actionUrl": "/search/assets/{userId}",
          "authorityName": "ASSETS_BY_USER_ID"
        },
        {
          "aaId": 175,
          "name": "Create Alert Subscribe",
          "menuId": 49,
          "parentAAId": 0,
          "actionUrl": "/ams/alerts/subscribe",
          "authorityName": "CREATE_ALERT_SUBSCRIBE"
        },
        {
          "aaId": 177,
          "name": "Delete Alert Subscribe",
          "menuId": 49,
          "parentAAId": 0,
          "actionUrl": "/ams/alerts/{alphanumspecial}/subscribe",
          "authorityName": "Delete_ALERT_SUBSCRIBE"
        },
        {
          "aaId": 178,
          "name": "Get Alert Subscribe List",
          "menuId": 49,
          "parentAAId": 0,
          "actionUrl": "/ams/alerts/subscribe/list",
          "authorityName": "GET_ALERT_SUBSCRIBE_LIST"
        },
        {
          "aaId": 174,
          "name": "Get Alert Type List",
          "menuId": 49,
          "parentAAId": 0,
          "actionUrl": "/ams/alertstypes/list",
          "authorityName": "GET_ALERT_TYPE_LIST"
        },
        {
          "aaId": 180,
          "name": "Search AMS Assets by User Id",
          "menuId": 49,
          "parentAAId": 0,
          "actionUrl": "/search/assets/ams/{userId}",
          "authorityName": "SEARCH_AMS_ASSETS_BY_USER_ID"
        },
        {
          "aaId": 176,
          "name": "Update Alert Subscribe",
          "menuId": 49,
          "parentAAId": 0,
          "actionUrl": "/ams/alerts/subscribe",
          "authorityName": "UPDATE_ALERT_SUBSCRIBE"
        },
        {
          "aaId": 324,
          "name": "Assets Model: Create Alert Subscribe",
          "menuId": 49,
          "parentAAId": 0,
          "actionUrl": "/platform/ams/subscriptions",
          "authorityName": "CREATE_AM_ALERT_SUBSCRIBE"
        },
        {
          "aaId": 326,
          "name": "Assets Model: Delete Alert Subscribe",
          "menuId": 49,
          "parentAAId": 0,
          "actionUrl": "/platform/ams/subscriptions/{alphanumspecial}",
          "authorityName": "Delete_AM_ALERT_SUBSCRIBE"
        },
        {
          "aaId": 327,
          "name": "Assets Model: Get Alert Subscribe List",
          "menuId": 49,
          "parentAAId": 0,
          "actionUrl": "/platform/ams/subscriptions",
          "authorityName": "GET_AM_ALERT_SUBSCRIBE_LIST"
        },
        {
          "aaId": 325,
          "name": "Assets Model: Update Alert Subscribe",
          "menuId": 49,
          "parentAAId": 0,
          "actionUrl": "/platform/ams/subscriptions/{alphanumspecial}",
          "authorityName": "UPDATE_AM_ALERT_SUBSCRIBE"
        },
        {
          "aaId": 328,
          "name": "Assets Model: Get Alert Type List",
          "menuId": 49,
          "parentAAId": 0,
          "actionUrl": "/platform/ams/alertsTypes",
          "authorityName": "GET_AM_ALERT_TYPE_LIST"
        }
      ]
    },
    {
      "menuId": 40,
      "name": "Daily Report",
      "menuUrl": "/dailyreport",
      "authorities": [
        {
          "aaId": 464,
          "name": "Download DG Daily Report",
          "menuId": 40,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/shift/download/dg",
          "authorityName": "DG_DAILY_SHIFTS_REPORT_DOWNLOAD"
        },
        {
          "aaId": 463,
          "name": "DG Daily Report",
          "menuId": 40,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/shifts/dg",
          "authorityName": "DG_DAILY_SHIFTS_REPORT"
        },
        {
          "aaId": 465,
          "name": "Vehicle Daily Shifts Report",
          "menuId": 40,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/shifts/vehicle",
          "authorityName": "VEHICLE_DAILY_SHIFTS_REPORT"
        },
        {
          "aaId": 466,
          "name": "Download Vehicle Daily Shifts Report",
          "menuId": 40,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/shift/download/vehicle",
          "authorityName": "VEHICLE_DAILY_SHIFTS_REPORT_DOWNLOAD"
        }
      ]
    },
    {
      "menuId": 41,
      "name": "Trace device Logs",
      "menuUrl": "/devicelogs",
      "authorities": [
        {
          "aaId": 138,
          "name": "Get Assigned Asset List",
          "menuId": 41,
          "parentAAId": 0,
          "actionUrl": "/asset/user",
          "authorityName": "GET_ASSIGNED_ASSET_LIST"
        },
        {
          "aaId": 139,
          "name": "Get Event List",
          "menuId": 41,
          "parentAAId": 0,
          "actionUrl": "/report/eventcode",
          "authorityName": "GET_EVENT_LIST"
        },
        {
          "aaId": 140,
          "name": "Get Raw History Data",
          "menuId": 41,
          "parentAAId": 0,
          "actionUrl": "/reports/history/raw/data",
          "authorityName": "GET_RAW_HISTORY_DATA"
        }
      ]
    },
    {
      "menuId": 36,
      "name": "Asset History",
      "menuUrl": "/assethistory",
      "authorities": [
        {
          "aaId": 75,
          "name": "Asset Alert History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/history/Alerts",
          "authorityName": "ASSET_ALERT_HISTORY"
        },
        {
          "aaId": 84,
          "name": "Asset Battery History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/history/Battery",
          "authorityName": "ASSET_BATTERY_HISTORY"
        },
        {
          "aaId": 83,
          "name": "Asset Door History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/history/Door",
          "authorityName": "ASSET_DOOR_HISTORY"
        },
        {
          "aaId": 76,
          "name": "Asset Fuel History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/history/Fuel",
          "authorityName": "ASSET_FUEL_HISTORY"
        },
        {
          "aaId": 77,
          "name": "Asset Location History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/history/Location",
          "authorityName": "ASSET_LOCATION_HISTORY"
        },
        {
          "aaId": 82,
          "name": "Asset Power History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/history/Power",
          "authorityName": "ASSET_POWER_HISTORY"
        },
        {
          "aaId": 81,
          "name": "Asset Pressure History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/history/Pressure",
          "authorityName": "ASSET_PRESSURE_HISTORY"
        },
        {
          "aaId": 78,
          "name": "Asset Sli History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/history/SLI",
          "authorityName": "ASSET_SLI_HISTORY"
        },
        {
          "aaId": 80,
          "name": "Asset Temperature History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/history/Temperature",
          "authorityName": "ASSET_TEMPERATURE_HISTORY"
        },
        {
          "aaId": 118,
          "name": "Search Vehicle",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/search/assets",
          "authorityName": "SEARCH_VEHICLE"
        },
        {
          "aaId": 246,
          "name": "Download Report: Location",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/location/download",
          "authorityName": "REPORT_DOWNLOAD_LOCATION"
        },
        {
          "aaId": 247,
          "name": "Download Report: Alerts",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/alerts/download",
          "authorityName": "REPORT_DOWNLOAD_ALERTS"
        },
        {
          "aaId": 251,
          "name": "Download Report: Battery",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/battery/download",
          "authorityName": "REPORT_DOWNLOAD_BATTERY"
        },
        {
          "aaId": 250,
          "name": "Download Report: Door",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/door/download",
          "authorityName": "REPORT_DOWNLOAD_DOOR"
        },
        {
          "aaId": 248,
          "name": "Download Report: Fuel",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/fuel/download",
          "authorityName": "REPORT_DOWNLOAD_FUEL"
        },
        {
          "aaId": 249,
          "name": "Download Report: Power",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/power/download",
          "authorityName": "REPORT_DOWNLOAD_POWER"
        },
        {
          "aaId": 254,
          "name": "Download Report: Pressure",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/pressure/download",
          "authorityName": "REPORT_DOWNLOAD_PRESSURE"
        },
        {
          "aaId": 253,
          "name": "Download Report: SLI",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/sli/download",
          "authorityName": "REPORT_DOWNLOAD_SLI"
        },
        {
          "aaId": 252,
          "name": "Download Report: Temperature",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/temperature/download",
          "authorityName": "REPORT_DOWNLOAD_TEMPERATURE"
        },
        {
          "aaId": 267,
          "name": "Asset Vehicle Status History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/history/vehiclestatus",
          "authorityName": "ASSET_VEHICLE_STATUS_HISTORY"
        },
        {
          "aaId": 268,
          "name": "Download Report: Vehicle Status",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/vehiclestatus/download",
          "authorityName": "REPORT_DOWNLOAD_VEHICLE_STATUS"
        },
        {
          "aaId": 369,
          "name": "Assets Model: Asset History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/platform/reports/rawdata/{alphanumspecial}",
          "authorityName": "GET_AM_ASSET_HISTORY"
        },
        {
          "aaId": 422,
          "name": "Assets Model: Asset History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/{alphanumspecial}",
          "authorityName": "GET_AM_ASSET_HISTORY"
        },
        {
          "aaId": 480,
          "name": "V1 Asset Asset Status History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/assetstatus",
          "authorityName": "V1_ASSET_ASSETSTATUS_HISTORY"
        },
        {
          "aaId": 473,
          "name": "V1 Asset Fuel History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/fuel",
          "authorityName": "V1_ASSET_FUEL_HISTORY"
        },
        {
          "aaId": 477,
          "name": "V1 Asset Power History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/power",
          "authorityName": "V1_ASSET_POWER_HISTORY"
        },
        {
          "aaId": 481,
          "name": "V1 Asset Location History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/location",
          "authorityName": "V1_ASSET_LOCATION_HISTORY"
        },
        {
          "aaId": 476,
          "name": "V1 Asset Pressure History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/pressure",
          "authorityName": "V1_ASSET_PRESSURE_HISTORY"
        },
        {
          "aaId": 474,
          "name": "V1 Asset Sli History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/sli",
          "authorityName": "V1_ASSET_SLI_HISTORY"
        },
        {
          "aaId": 475,
          "name": "V1 Asset Temperature History",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/temperature",
          "authorityName": "V1_ASSET_TEMPERATURE_HISTORY"
        },
        {
          "aaId": 489,
          "name": "V1 Download Report: Asset Status",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/download/assetstatus",
          "authorityName": "V1_REPORT_DOWNLOAD_ASSETSTATUS"
        },
        {
          "aaId": 482,
          "name": "V1 Download Report: Fuel",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/download/fuel",
          "authorityName": "V1_REPORT_DOWNLOAD_FUEL"
        },
        {
          "aaId": 490,
          "name": "V1 Download Report: Location",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/download/location",
          "authorityName": "V1_REPORT_DOWNLOAD_LOCATION"
        },
        {
          "aaId": 486,
          "name": "V1 Download Report: Power",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/download/power",
          "authorityName": "V1_REPORT_DOWNLOAD_POWER"
        },
        {
          "aaId": 485,
          "name": "V1 Download Report: Pressure",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/download/pressure",
          "authorityName": "V1_REPORT_DOWNLOAD_PRESSURE"
        },
        {
          "aaId": 483,
          "name": "V1 Download Report: Sli",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/download/sli",
          "authorityName": "V1_REPORT_DOWNLOAD_SLI"
        },
        {
          "aaId": 484,
          "name": "V1 Download Report: Temperature",
          "menuId": 36,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/rawdata/download/temperature",
          "authorityName": "V1_REPORT_DOWNLOAD_TEMPERATURE"
        }
      ]
    },
    {
      "menuId": 62,
      "name": "Overspeeding Report",
      "menuUrl": "/reports/overspeed",
      "authorities": [
        {
          "aaId": 292,
          "name": "Download: Geofence Overspeed Report",
          "menuId": 62,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/download/geofenceOverSpeed",
          "authorityName": "DOWNLOAD_GEOFENCE_OVERSPEEDING_REPORT"
        },
        {
          "aaId": 291,
          "name": "Download: Global Overspeed Report",
          "menuId": 62,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/download/globalOverSpeed",
          "authorityName": "DOWNLOAD_GLOBAL_OVERSPEEDING_REPORT"
        },
        {
          "aaId": 470,
          "name": "Report: Geofence Overspeed",
          "menuId": 62,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/geofenceOverSpeed",
          "authorityName": "REPORT_GEOFENCE_OVERSPEED"
        },
        {
          "aaId": 471,
          "name": "Report: Geofence Overspeed",
          "menuId": 62,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/geofenceOverSpeed",
          "authorityName": "REPORT_GEOFENCE_OVERSPEED"
        },
        {
          "aaId": 469,
          "name": "Report: Global Overspeed",
          "menuId": 62,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/globalOverSpeed",
          "authorityName": "REPORT_GLOBAL_OVERSPEED"
        }
      ]
    },
    {
      "menuId": 35,
      "name": "Trip Analysis (Ignition Cycle)",
      "menuUrl": "/tripAnalysis/ignitioncycle",
      "authorities": [
        {
          "aaId": 117,
          "name": "Search Vehicle",
          "menuId": 35,
          "parentAAId": 0,
          "actionUrl": "/search/assets",
          "authorityName": "SEARCH_VEHICLE"
        },
        {
          "aaId": 157,
          "name": "Trip Route Preview",
          "menuId": 35,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/route",
          "authorityName": "TRIP_ROUTE_PREVIEW"
        },
        {
          "aaId": 74,
          "name": "Vehicle Trip Info",
          "menuId": 35,
          "parentAAId": 0,
          "actionUrl": "/reports/tripslist",
          "authorityName": "VEHICLE_TRIP_INFO"
        },
        {
          "aaId": 194,
          "name": "Download Report",
          "menuId": 35,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/ignitioncycle/download",
          "authorityName": "REPORT_TRIPS_IGNITIONCYCLE_DOWNLOAD"
        },
        {
          "aaId": 417,
          "name": "Assets Model: Ignition Trip Reports",
          "menuId": 35,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/trips/ignitioncycle",
          "authorityName": "AM_IGNITION_TRIP_REPORTS"
        },
        {
          "aaId": 434,
          "name": "Assets Model: Trip Route Preview",
          "menuId": 35,
          "parentAAId": 0,
          "actionUrl": "/v1/route/preview",
          "authorityName": "AM_TRIP_ROUTE_PREVIEW"
        },
        {
          "aaId": 436,
          "name": "Assets Model: Ignition Trip Download Reports",
          "menuId": 35,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/trips/download/ignitioncycle",
          "authorityName": "AM_IGNITION_TRIP_DOWNLOAD_REPORTS"
        }
      ]
    },
    {
      "menuId": 52,
      "name": "Trip Analysis (Route)",
      "menuUrl": "/tripAnalysis/route",
      "authorities": [
        {
          "aaId": 197,
          "name": "Vehicle Trip Info",
          "menuId": 52,
          "parentAAId": 0,
          "actionUrl": "/reports/tripslist",
          "authorityName": "VEHICLE_TRIP_INFO"
        },
        {
          "aaId": 198,
          "name": "Search Vehicle",
          "menuId": 52,
          "parentAAId": 0,
          "actionUrl": "/search/assets",
          "authorityName": "SEARCH_VEHICLE"
        },
        {
          "aaId": 199,
          "name": "Trip Route Preview",
          "menuId": 52,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/route",
          "authorityName": "TRIP_ROUTE_PREVIEW"
        },
        {
          "aaId": 200,
          "name": "Download Report",
          "menuId": 52,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/route/download",
          "authorityName": "REPORT_TRIPS_ROUTE_DOWNLOAD"
        },
        {
          "aaId": 416,
          "name": "Assets Model: Route Trip Reports",
          "menuId": 52,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/trips/route",
          "authorityName": "AM_ROUTE_TRIP_REPORTS"
        },
        {
          "aaId": 435,
          "name": "Assets Model: Route Trip Download Reports",
          "menuId": 52,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/trips/download/route",
          "authorityName": "AM_ROUTE_TRIP_DOWNLOAD_REPORTS"
        },
        {
          "aaId": 491,
          "name": "Assets Model: Trip Route Preview",
          "menuId": 52,
          "parentAAId": 0,
          "actionUrl": "/v1/route/preview",
          "authorityName": "AM_TRIP_ROUTE_PREVIEW"
        }
      ]
    },
    {
      "menuId": 61,
      "name": "Trip Analysis (Route)-V2",
      "menuUrl": "/tripAnalysis/combinedroute",
      "authorities": [
        {
          "aaId": 285,
          "name": "Download Report",
          "menuId": 61,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/combinedroute/download",
          "authorityName": "REPORT_TRIPS_COMBINED_ROUTE_DOWNLOAD"
        },
        {
          "aaId": 283,
          "name": "Search Vehicle",
          "menuId": 61,
          "parentAAId": 0,
          "actionUrl": "/search/assets",
          "authorityName": "SEARCH_VEHICLE"
        },
        {
          "aaId": 284,
          "name": "Trip Route Preview",
          "menuId": 61,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/route",
          "authorityName": "TRIP_ROUTE_PREVIEW"
        },
        {
          "aaId": 282,
          "name": "Vehicle Trip Info",
          "menuId": 61,
          "parentAAId": 0,
          "actionUrl": "/reports/tripslist",
          "authorityName": "VEHICLE_TRIP_INFO"
        },
        {
          "aaId": 419,
          "name": "Assets Model: Combined Route Reports",
          "menuId": 61,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/trips/combinedroute",
          "authorityName": "AM_COMBINED_ROUTE_REPORTS"
        },
        {
          "aaId": 438,
          "name": "Assets Model: Combined Route Download Reports",
          "menuId": 61,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/trips/download/combinedroute",
          "authorityName": "AM_COMBINED_ROUTE_DOWNLOAD_REPORTS"
        },
        {
          "aaId": 493,
          "name": "Assets Model: Trip Route Preview",
          "menuId": 61,
          "parentAAId": 0,
          "actionUrl": "/v1/route/preview",
          "authorityName": "AM_TRIP_ROUTE_PREVIEW"
        }
      ]
    },
    {
      "menuId": 45,
      "name": "Geofence Report",
      "menuUrl": "/reports/geofence",
      "authorities": [
        {
          "aaId": 158,
          "name": "Vehicle Trip Info",
          "menuId": 45,
          "parentAAId": 0,
          "actionUrl": "/reports/tripslist",
          "authorityName": "VEHICLE_TRIP_INFO"
        },
        {
          "aaId": 196,
          "name": "Download Report",
          "menuId": 45,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/geofence/download",
          "authorityName": "REPORT_TRIPS_GEOFENCE_DOWNLOAD"
        },
        {
          "aaId": 418,
          "name": "Assets Model: Geofence Trip Reports",
          "menuId": 45,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/trips/geofence",
          "authorityName": "AM_GEOFENCE_TRIP_REPORTS"
        },
        {
          "aaId": 437,
          "name": "Assets Model: Geofence Trip Download Reports",
          "menuId": 45,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/trips/download/geofence",
          "authorityName": "AM_GEOFENCE_TRIP_DOWNLOAD_REPORTS"
        }
      ]
    },
    {
      "menuId": 3,
      "name": "User Management",
      "menuUrl": "/users",
      "authorities": [
        {
          "aaId": 71,
          "name": "Change Password",
          "menuId": 3,
          "parentAAId": 0,
          "actionUrl": "/users/updatepassword",
          "authorityName": "CHANGE_PASSWORD"
        },
        {
          "aaId": 46,
          "name": "Get All Authorities to User",
          "menuId": 3,
          "parentAAId": 0,
          "actionUrl": "/rolepermission/role/{id}/{userId}/authorities",
          "authorityName": "READ_AUTHORITIES_TO_USER"
        },
        {
          "aaId": 8,
          "name": "Get User By Id",
          "menuId": 3,
          "parentAAId": 0,
          "actionUrl": "/users/{id}",
          "authorityName": "READ_USER_BY_ID"
        },
        {
          "aaId": 9,
          "name": "Get User By Name",
          "menuId": 3,
          "parentAAId": 0,
          "actionUrl": "/users/name/{email}",
          "authorityName": "READ_USER_BY_NAME"
        },
        {
          "aaId": 88,
          "name": "Password Policy",
          "menuId": 3,
          "parentAAId": 0,
          "actionUrl": "/users/passwordpolicy",
          "authorityName": "PASSWORD_POLICY"
        },
        {
          "aaId": 113,
          "name": "Roles Master",
          "menuId": 3,
          "parentAAId": 0,
          "actionUrl": "/roles",
          "authorityName": "ROLE_MASTER"
        },
        {
          "aaId": 111,
          "name": "Site Master",
          "menuId": 3,
          "parentAAId": 0,
          "actionUrl": "/sites",
          "authorityName": "SITE_MASTER"
        },
        {
          "aaId": 112,
          "name": "Tenant Master",
          "menuId": 3,
          "parentAAId": 0,
          "actionUrl": "/tenants",
          "authorityName": "TENANT_MASTER"
        },
        {
          "aaId": 11,
          "name": "Update User",
          "menuId": 3,
          "parentAAId": 0,
          "actionUrl": "/users",
          "authorityName": "UPDATE_USER"
        },
        {
          "aaId": 7,
          "name": "User List",
          "menuId": 3,
          "parentAAId": 0,
          "actionUrl": "/users",
          "authorityName": "READ_USER_LIST"
        },
        {
          "aaId": 47,
          "name": "Assign Authorities to User",
          "menuId": 3,
          "parentAAId": 0,
          "actionUrl": "/rolepermission/user/authorities",
          "authorityName": "UPDATE_AUTHORITIES_TO_USER"
        },
        {
          "aaId": 256,
          "name": "Asset Ownership for Multiple Assets(s)",
          "menuId": 3,
          "parentAAId": 0,
          "actionUrl": "/platform/assets/bulk/uids",
          "authorityName": "ASSET_OWNERSHIP_MULTI_ASSETS_PLATFORM"
        },
        {
          "aaId": 255,
          "name": "Assets Model: Get Assets List",
          "menuId": 3,
          "parentAAId": 0,
          "actionUrl": "/platform/assets",
          "authorityName": "GET_ASSETS_PLATFORM"
        },
        {
          "aaId": 243,
          "name": "Get Individual User",
          "menuId": 3,
          "parentAAId": 0,
          "actionUrl": "/individual/users",
          "authorityName": "GET_INDIVIDUAL_USER"
        }
      ]
    },
    {
      "menuId": 46,
      "name": "Alert Report",
      "menuUrl": "/reports/alert",
      "authorities": [
        {
          "aaId": 159,
          "name": "Asset Alert History",
          "menuId": 46,
          "parentAAId": 0,
          "actionUrl": "/reports/history/Alerts",
          "authorityName": "ASSET_ALERT_HISTORY"
        },
        {
          "aaId": 424,
          "name": "Assets Model: Alert Reports",
          "menuId": 46,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/alerts",
          "authorityName": "AM_ALERT_REPORTS"
        },
        {
          "aaId": 472,
          "name": "Download Report: Alerts",
          "menuId": 46,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/download/alerts",
          "authorityName": "V1_REPORT_DOWNLOAD_FOR_ALERTS"
        }
      ]
    },
    {
      "menuId": 54,
      "name": "Assets Management",
      "menuUrl": "/assetsmanagement",
      "authorities": [
        {
          "aaId": 222,
          "name": "Assets Model: Get Assets List",
          "menuId": 54,
          "parentAAId": 0,
          "actionUrl": "/platform/assets",
          "authorityName": "GET_ASSETS_PLATFORM"
        },
        {
          "aaId": 238,
          "name": "Add Device Into Asset",
          "menuId": 54,
          "parentAAId": 0,
          "actionUrl": "/platform/assets/{alphanumspecial}/dids",
          "authorityName": "ADD_DEVICE_INTO_ASSET_PLATFORM"
        },
        {
          "aaId": 226,
          "name": "Asset Ownership for Multiple User(s)",
          "menuId": 54,
          "parentAAId": 0,
          "actionUrl": "/platform/assets/{alphanumspecial}/uids",
          "authorityName": "ASSET_OWNERSHIP_MULTI_USER_PLATFORM"
        },
        {
          "aaId": 223,
          "name": "Create Asset",
          "menuId": 54,
          "parentAAId": 0,
          "actionUrl": "/platform/assets",
          "authorityName": "CREATE_ASSETS_PLATFORM"
        },
        {
          "aaId": 224,
          "name": "Get Assets Categories",
          "menuId": 54,
          "parentAAId": 0,
          "actionUrl": "/platform/assets/categories",
          "authorityName": "GET_ASSETS_CATEGORIES_PLATFORM"
        },
        {
          "aaId": 221,
          "name": "Get Device List",
          "menuId": 54,
          "parentAAId": 0,
          "actionUrl": "/platform/devices",
          "authorityName": "GET_DEVICES_PLATFORM"
        },
        {
          "aaId": 245,
          "name": "Get Individual User",
          "menuId": 54,
          "parentAAId": 0,
          "actionUrl": "/individual/users",
          "authorityName": "GET_INDIVIDUAL_USER"
        },
        {
          "aaId": 225,
          "name": "Unassign Device from Asset",
          "menuId": 54,
          "parentAAId": 0,
          "actionUrl": "/platform/assets/{alphanumspecial}/dids/{alphanumspecialwithcomma}",
          "authorityName": "UNASSIGN_DEVICE_FROM_ASSET_PLATFORM"
        },
        {
          "aaId": 233,
          "name": "Update Asset",
          "menuId": 54,
          "parentAAId": 0,
          "actionUrl": "/platform/assets/{alphanumspecial}",
          "authorityName": "UPDATE_ASSET_PLATFORM"
        }
      ]
    },
    {
      "menuId": 4,
      "name": "Vehicle Management",
      "menuUrl": "/device-management",
      "authorities": [
        {
          "aaId": 21,
          "name": "Assign Device",
          "menuId": 4,
          "parentAAId": 0,
          "actionUrl": "/devices",
          "authorityName": "CREATE_VEHICLE"
        },
        {
          "aaId": 115,
          "name": "Fuel Type",
          "menuId": 4,
          "parentAAId": 0,
          "actionUrl": "/devices/fuelType",
          "authorityName": "FUEL_TYPE"
        },
        {
          "aaId": 116,
          "name": "Get Assign Vehicle By Id",
          "menuId": 4,
          "parentAAId": 0,
          "actionUrl": "/devices/associatedvehicle/{id}",
          "authorityName": "GET_ASSIGN_VEHICLE_BY_ID"
        },
        {
          "aaId": 114,
          "name": "Get Assign Vehicle List",
          "menuId": 4,
          "parentAAId": 0,
          "actionUrl": "/devices/associatedvehicles",
          "authorityName": "READ_VEHICLE_LIST"
        },
        {
          "aaId": 62,
          "name": "Get Unassign IMEI List By UserId",
          "menuId": 4,
          "parentAAId": 0,
          "actionUrl": "/devices/unassignimei/{id}",
          "authorityName": "GET_UNASSIGN_IMEI_LIST_BY_ID"
        },
        {
          "aaId": 20,
          "name": "Unassign Device",
          "menuId": 4,
          "parentAAId": 0,
          "actionUrl": "/devices/{alphanum}",
          "authorityName": "DELETE_DEVICE"
        },
        {
          "aaId": 30,
          "name": "Update Device",
          "menuId": 4,
          "parentAAId": 0,
          "actionUrl": "/devices",
          "authorityName": "UPDATE_VEHICLE"
        }
      ]
    },
    {
      "menuId": 9,
      "name": "Geofencing",
      "menuUrl": "/geofencing",
      "authorities": [
        {
          "aaId": 107,
          "name": "Assign Vehicle To Geofence",
          "menuId": 9,
          "parentAAId": 0,
          "actionUrl": "/geofence/vehicle",
          "authorityName": "ASSIGN_VEHICLE_TO_GEOFENCE"
        },
        {
          "aaId": 259,
          "name": "Assets Model: Unassign Asset To Geofence",
          "menuId": 9,
          "parentAAId": 0,
          "actionUrl": "/platform/assets/bulk/geofences/delete",
          "authorityName": "UNASSIGN_ASSET_TO_GEOFENCE_PLATFORM"
        },
        {
          "aaId": 260,
          "name": "Assets Model: All Assets Unassign from Geofence",
          "menuId": 9,
          "parentAAId": 0,
          "actionUrl": "/platform/geofences/{alphanumspecial}/assets/all",
          "authorityName": "ALL_ASSETS_UNASSIGN_GEOFENCE_PLATFORM"
        },
        {
          "aaId": 258,
          "name": "Assets Model: Assign Assets To Geofence",
          "menuId": 9,
          "parentAAId": 0,
          "actionUrl": "/platform/assets/bulk/geofences",
          "authorityName": "ASSIGN_ASSETS_TO_GEOFENCE_PLATFORM"
        },
        {
          "aaId": 263,
          "name": "Assets Model: Create Geofence",
          "menuId": 9,
          "parentAAId": 0,
          "actionUrl": "/platform/geofences",
          "authorityName": "CREATE_GEOFENCE_PLATFORM"
        },
        {
          "aaId": 266,
          "name": "Assets Model: Delete Geofence",
          "menuId": 9,
          "parentAAId": 0,
          "actionUrl": "/platform/geofences/{alphanumspecial}",
          "authorityName": "DELETE_GEOFENCE_PLATFORM"
        },
        {
          "aaId": 257,
          "name": "Assets Model: Get Assets List",
          "menuId": 9,
          "parentAAId": 0,
          "actionUrl": "/platform/assets",
          "authorityName": "GET_ASSETS_PLATFORM"
        },
        {
          "aaId": 270,
          "name": "Assets Model: Get Geofence Alert Me On",
          "menuId": 9,
          "parentAAId": 0,
          "actionUrl": "/geofence/geofenceassigntype",
          "authorityName": "GET_GEOFENCE_ALERT_ME_ON_PLATFORM"
        },
        {
          "aaId": 286,
          "name": "Assets Model: Get Geofence by Id",
          "menuId": 9,
          "parentAAId": 0,
          "actionUrl": "/platform/geofences/{alphanumspecial}",
          "authorityName": "GET_GEOFENCE_BY_ID_PLATFORM"
        },
        {
          "aaId": 271,
          "name": "Assets Model: Get Geofence Type",
          "menuId": 9,
          "parentAAId": 0,
          "actionUrl": "/geofence/geofencetypes",
          "authorityName": "GET_GEOFENCE_TYPE_PLATFORM"
        },
        {
          "aaId": 265,
          "name": "Assets Model: Get Geofences",
          "menuId": 9,
          "parentAAId": 0,
          "actionUrl": "/platform/geofences",
          "authorityName": "GET_GEOFENCES_PLATFORM"
        },
        {
          "aaId": 264,
          "name": "Assets Model: Update Geofence",
          "menuId": 9,
          "parentAAId": 0,
          "actionUrl": "/platform/geofences/{alphanumspecial}",
          "authorityName": "UPDATE_GEOFENCE_PLATFORM"
        }
      ]
    },
    {
      "menuId": 51,
      "name": "Route Management",
      "menuUrl": "/route",
      "authorities": [
        {
          "aaId": 192,
          "name": "Assign Route Vehicle",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/route/assignvehicle",
          "authorityName": "ASSIGN_ROUTE_VEHICLE"
        },
        {
          "aaId": 193,
          "name": "Bulk Unassign Vehicle To Route",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/route/unassignvehicle/{id}",
          "authorityName": "BULK_UNASSIGN_VEHICLE_TO_ROUTE"
        },
        {
          "aaId": 187,
          "name": "Create Route",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/route",
          "authorityName": "CREATE_ROUTE"
        },
        {
          "aaId": 190,
          "name": "Delete Route",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/route/{id}",
          "authorityName": "DELETE_ROUTE"
        },
        {
          "aaId": 191,
          "name": "Get Assigned Route Vehicles",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/route/getVehicles",
          "authorityName": "GET_ASSIGNED_ROUTE_VEHICLES"
        },
        {
          "aaId": 189,
          "name": "Get Route",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/route",
          "authorityName": "GET_ROUTE"
        },
        {
          "aaId": 188,
          "name": "Update Route",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/route",
          "authorityName": "UPDATE_ROUTE"
        },
        {
          "aaId": 204,
          "name": "Get Geofence by Enterprise",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/enterprise/geofence",
          "authorityName": "GET_GEOFENCE_BY_ENTERPRISE"
        },
        {
          "aaId": 218,
          "name": "Route With Vehicle Assignment",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/routevehicle/{id}",
          "authorityName": "ROUTE_WITH_VEHICLE_ASSIGNMENT"
        },
        {
          "aaId": 294,
          "name": "Assets Model: Create Route",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/platform/rms/routes",
          "authorityName": "CREATE_ROUTE_PLATFORM"
        },
        {
          "aaId": 298,
          "name": "Assets Model: Delete Route",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/platform/rms/routes/{alphanumspecial}",
          "authorityName": "DELETE_ROUTES_PLATFORM"
        },
        {
          "aaId": 293,
          "name": "Assets Model: Get Geofences",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/platform/geofences",
          "authorityName": "GET_GEOFENCES_PLATFORM"
        },
        {
          "aaId": 296,
          "name": "Assets Model: Get Route by Id",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/platform/rms/routes/{alphanumspecial}",
          "authorityName": "GET_ROUTE_BY_ID_PLATFORM"
        },
        {
          "aaId": 297,
          "name": "Assets Model: Get Routes",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/platform/rms/routes",
          "authorityName": "GET_ROUTES_PLATFORM"
        },
        {
          "aaId": 299,
          "name": "Assets Model: Unassign All Assets from a Route",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/platform/rms/routes/{alphanumspecial}/assets/all",
          "authorityName": "UNASSIGN_ALL_ASSETS_FROM_ROUTE_PLATFORM"
        },
        {
          "aaId": 295,
          "name": "Assets Model: Update Route Or Assign Assets to Route",
          "menuId": 51,
          "parentAAId": 0,
          "actionUrl": "/platform/rms/routes/{alphanumspecial}",
          "authorityName": "UPDATE_ROUTE_OR_ASSIGN_ASSETS_PLATFORM"
        }
      ]
    },
    {
      "menuId": 53,
      "name": "Session Report",
      "menuUrl": "/sessionreport",
      "authorities": [
        {
          "aaId": 216,
          "name": "Download Report",
          "menuId": 53,
          "parentAAId": 0,
          "actionUrl": "/reports/trips/dgfuelsession/download",
          "authorityName": "REPORT_TRIPS_DGFUEL_DGFUELSESSION_DOWNLOAD"
        },
        {
          "aaId": 215,
          "name": "Search Vehicle",
          "menuId": 53,
          "parentAAId": 0,
          "actionUrl": "/search/assets",
          "authorityName": "SEARCH_VEHICLE"
        },
        {
          "aaId": 214,
          "name": "Session Report",
          "menuId": 53,
          "parentAAId": 0,
          "actionUrl": "/reports/history/dgfuel/session",
          "authorityName": "SESSION_REPORT"
        },
        {
          "aaId": 421,
          "name": "Assets Model: Session Based Reports",
          "menuId": 53,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/session/{alphanumspecial}",
          "authorityName": "AM_SESSION_BASED_REPORTS"
        },
        {
          "aaId": 459,
          "name": "DG Session Report",
          "menuId": 53,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/session/dg",
          "authorityName": "DG_SESSION_REPORT"
        },
        {
          "aaId": 460,
          "name": "Download DG Session Report",
          "menuId": 53,
          "parentAAId": 0,
          "actionUrl": "/v1/reports/session/download/dg",
          "authorityName": "DOWNLOAD_DG_SESSION_REPORT"
        }
      ]
    },
    {
      "menuId": 63,
      "name": "Dashboard Configuration",
      "menuUrl": "/dashboard/config",
      "authorities": [
        {
          "aaId": 310,
          "name": "Update Config",
          "menuId": 63,
          "parentAAId": 0,
          "actionUrl": "/dashboard/config",
          "authorityName": "UPDATE_CONFIG"
        }
      ]
    }
  ]

  // en dof charts
  constructor(public dialog: MatDialog,  private renderer: Renderer2, private toast: NgAlertService) {}

  toggle() {
    this.isOpen = !this.isOpen;
  }
  hideMenu() {
    this.renderer.removeClass(document.body, 'menu-open');
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(DialogOverviewExampleDialogComponent, {
      width: '250px',
      data: {name: 'sundar madhav das', animal: 'human'}
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.animal = result;
    });
  }  

  
  createSuccessMessage(): void {
    this.toast.createSuccessAlert('Toast success message');
  }

  createDangerMessage(): void {
    this.toast.createDangerAlert('Toast error message');
  }

  createWarningMessage(): void {
    this.toast.createWarningAlert('Toast warning message');
  }

  createInfoMessage(): void {
    this.toast.createInfoAlert('Toast info message');
  }

}
