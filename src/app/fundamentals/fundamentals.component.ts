import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-fundamentals',
  templateUrl: './fundamentals.component.html',
  styleUrls: ['./fundamentals.component.css']
}) 
export class FundamentalsComponent implements OnInit {

  constructor() { }
  date = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());
  ngOnInit() {
  }

}
