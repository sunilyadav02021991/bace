import { Component, HostBinding, Input } from '@angular/core';
import { trigger, transition, useAnimation } from '@angular/animations';
import { pulseAnimation } from '../pulseAnimation';

@Component({
  selector: 'app-clap-fab',
  template: `<i class="material-icons" [@openClose]="isOpen ? 'open' : 'counterChange'" (click)="toggle()">pan_tool</i>`,
  animations: [
    trigger('counterChange', [
      transition(
        ':increment',
        useAnimation(pulseAnimation, {
          params: {
            timings: '400ms cubic-bezier(.11,.99,.83,.43)',
            scale: 1.05
          }
        })
      )
    ])
  ]
})
export class AppClabFabComponent {
  @HostBinding('@counterChange')
  @Input()
  counter: number;

  isOpen = true;
  toggle() {
    this.isOpen = !this.isOpen;
  }
}

