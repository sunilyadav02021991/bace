import { Component, OnInit } from '@angular/core';

import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { NgAlertService } from 'webdunia-toast';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  title = 'animations';
  isOpen = true;

  centered = false;
  disabled = false;
  unbounded = false;

  radius: number;
  color: string;
  animal: string;
  myName: string;

  // charts related

  public lineChartData: ChartDataSets[] = [
    { data: [65, 59, 80, 81, 56, 55, 40, 10, 20, 90, 10], label: 'Personality' },
  ];
  public lineChartLabels: Label[] = ['Character', 'Competency', 'Devotion', 'Surrender', 'Sense Control', 'Sadachar', 'Sadhana',"Seva", "Siddhanta", "Simplicity", "Sanskar"];
  public lineChartOptions: (ChartOptions) = {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'gray',
      backgroundColor: 'rgba(255,0,0,0.3)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'pie';
  // line, bar, pie, Doughnut Chart, Radar Chart,
  // Polar Area Chart, Bubble Chart and Scatter Chart ||https://www.npmjs.com/package/ng2-charts
  public lineChartPlugins = [];
  constructor(private toast: NgAlertService) { }

  ngOnInit() {
  }
  
  createSuccessMessage(): void {
    this.toast.createSuccessAlert('Toast success message');
  }

  createDangerMessage(): void {
    this.toast.createDangerAlert('Toast error message');
  }

  createWarningMessage(): void {
    this.toast.createWarningAlert('Toast warning message');
  }

  createInfoMessage(): void {
    this.toast.createInfoAlert('Toast info message');
  }

}
