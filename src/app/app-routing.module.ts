import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppClabFabComponent } from './app-clab-fab/app-clab-fab.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NotfoundComponent } from './notfound/notfound.component';
import { FundamentalsComponent } from './fundamentals/fundamentals.component';

const routes: Routes = [ 
  { path:"", component : AppClabFabComponent }, 
  { path :"dashboard", component: DashboardComponent }, 
  { path :"fundamentals", component: FundamentalsComponent }, 
  { path: "**", component:NotfoundComponent } 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
