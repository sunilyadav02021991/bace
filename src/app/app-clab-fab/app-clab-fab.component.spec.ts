import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppClabFabComponent } from './app-clab-fab.component';

describe('AppClabFabComponent', () => {
  let component: AppClabFabComponent;
  let fixture: ComponentFixture<AppClabFabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppClabFabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppClabFabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
